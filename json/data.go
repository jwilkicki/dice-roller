package json

import (
	"encoding/json"
	"io"
)

// RollRequest is a request for rolling a set of dice
type RollRequest map[string]RollSpec

// RollSpec specifies
type RollSpec struct {
	Sides         int `json:"sides"`
	NumberOfRolls int `json:"numberOfRolls"`
}

// RollResponse is the response for a RollRequest
type RollResponse map[string][]int

// NewRollRequest creates a RollRequest from the specified reader
func NewRollRequest(r io.Reader) (RollRequest, error) {
	var request RollRequest
	err := fromJSON(r, &request)
	return request, err
}

// NewRollResponse creates a RollResponse from the specified reader
func NewRollResponse(r io.Reader) (RollResponse, error) {
	var response RollResponse
	err := fromJSON(r, &response)
	return response, err
}

func fromJSON(r io.Reader, v interface{}) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(&v)
}

// ToJSON writes RollResponse to the writer as a JSON string
func (response RollResponse) ToJSON(w io.Writer) error {
	encoder := json.NewEncoder(w)
	return encoder.Encode(response)
}
