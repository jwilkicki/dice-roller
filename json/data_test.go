package json

import (
	"bytes"
	"fmt"
	"strings"
	"testing"
)

func TestRollRequest(t *testing.T) {
	tests := []struct {
		jsonString          string
		expectedRollRequest RollRequest
		expectedError       bool
	}{
		{
			`{
				"black": {
					"sides": 6,
					"numberOfRolls": 4
				},
				"white": {
					"sides": 6,
					"numberOfRolls": 3
				}
			}`,
			RollRequest{
				"black": RollSpec{6, 4},
				"white": RollSpec{6, 3},
			},
			false,
		},
	}

	for _, test := range tests {
		stringReader := strings.NewReader(test.jsonString)
		fmt.Printf("Parsing string: %v", test.jsonString)
		request, err := NewRollRequest(stringReader)
		if test.expectedError && err == nil {
			t.Errorf("%s expected error but didn't fail\n", test.jsonString)
		} else if !test.expectedError && err != nil {
			t.Errorf("%s did not expect error, but got one: %v\n", test.jsonString, err)
		} else {
			if !compareRollRequest(request, test.expectedRollRequest) {
				t.Errorf("Expected %v but got %v", test.expectedRollRequest, request)
			}
		}
	}
}

func compareRollRequest(x, y RollRequest) bool {
	if len(x) != len(y) {
		return false
	}
	for key, xRollSpec := range x {
		if yRollSpec, ok := y[key]; ok {
			if xRollSpec.Sides != yRollSpec.Sides {
				return false
			}
			if xRollSpec.NumberOfRolls != yRollSpec.NumberOfRolls {
				return false
			}
		} else {
			return false
		}
	}

	return true
}

func TestRollResponse(t *testing.T) {
	tests := []struct {
		underTest     RollResponse
		expectedJSON  string
		expectedError bool
	}{{
		RollResponse{
			"black": []int{1, 2, 3, 4, 5, 6},
			"white": []int{6, 5, 4, 3, 2, 1},
		},
		`{
			"black": [
				1,
				2,
				3,
				4,
				5,
				6
			],
			"white": [
				6,
				5,
				4,
				3,
				2,
				1
			]
		}`,
		false,
	},
	}

	for _, test := range tests {
		b := new(bytes.Buffer)
		test.underTest.ToJSON(b)
		underTestJSON := b.String()
		expectedResponse, err := NewRollResponse(strings.NewReader(test.expectedJSON))
		if err != nil {
			t.Errorf("Failed to parse expected JSON: %v\n", err)
		}

		underTestReparsed, err := NewRollResponse(strings.NewReader(underTestJSON))
		if err != nil {
			t.Errorf("Failed to parse JSON from underTest: %v", underTestReparsed)
		}
		if !compareRollResponse(underTestReparsed, expectedResponse) {
			t.Errorf("Expected %v, but got %v", expectedResponse, underTestReparsed)
		}
		if !compareRollResponse(test.underTest, underTestReparsed) {
			t.Errorf("Reparsed response %v did not match original object %v", underTestReparsed, test.underTest)
		}
	}
}

func compareRollResponse(x, y RollResponse) bool {
	if len(x) != len(y) {
		return false
	}
	for key, xResults := range x {
		yResults, ok := y[key]
		if !ok {
			return false
		}

		if len(xResults) != len(yResults) {
			return false
		}

		for i, xResult := range xResults {
			if xResult != yResults[i] {
				return false
			}
		}
	}
	return true
}
