package dice

import (
	"errors"
	"log"
	"math/rand"
	"time"
)

// DieRoller defines an interface for rolling a die a number of times and sending the
// results to a channel
type DieRoller interface {
	Roll(numberOfRolls int, results chan RollResult) error
}

// Die represents a die with a set number of sides
type Die int

// NewDie creates a new die
func NewDie(sides int) (Die, error) {
	if sides < 2 {
		return 0, errors.New("sides must be greater than or equal to 1")
	}

	return Die(sides), nil
}

// RollResult represents a response from Roll
type RollResult struct {
	Result []int
	Err    error
}

// Roll rolls a die the specified number of times and returns
// the results as a slice of ints on the provided channel
func (die Die) Roll(numberOfRolls int, results chan RollResult) {
	log.Printf("Rolling a %d-sided die %d times", die, numberOfRolls)
	if numberOfRolls < 1 {
		results <- RollResult{nil, errors.New("numberOfRolls must be at least one")}
	}
	rand.Seed(time.Now().UnixNano())
	min := 1
	rolls := make([]int, numberOfRolls)
	for i := 0; i < numberOfRolls; i++ {
		rolls[i] = rand.Intn(int(die)-min+1) + min
	}
	log.Printf("Finished rolling; sending results")
	results <- RollResult{rolls, nil}
}
