package dice

import (
	"testing"
)

func TestNewDice(t *testing.T) {
	tests := []struct {
		numberOfSides int
		expected      Die
		expectedError bool
	}{
		{
			1,
			0,
			true,
		},
		{
			2,
			Die(2),
			false,
		},
		{
			-1,
			0,
			true,
		},
		{
			0,
			0,
			true,
		},
	}

	for _, test := range tests {
		_, err := NewDie(test.numberOfSides)
		if err == nil && test.expectedError {
			t.Errorf("expected %d-sized die to fail", test.numberOfSides)
		}
	}
}

func TestRoll(t *testing.T) {
	tests := []struct {
		numberOfRolls int
		expectedError bool
	}{
		{0, true},
		{1, false},
		{2, false},
	}
	for _, test := range tests {
		die, err := NewDie(6)
		if err != nil {
			t.Fail()
		}
		resultChan := make(chan RollResult)

		go die.Roll(test.numberOfRolls, resultChan)

		result := <-resultChan

		if test.expectedError && result.Err == nil {
			t.Errorf("For numberOfRolls %d, expected error\n", test.numberOfRolls)
		} else if !test.expectedError && result.Err != nil {
			t.Errorf("For numberOfRolls %d, got unexpected error %v\n", test.numberOfRolls, result.Err)
		} else {
			if len(result.Result) != test.numberOfRolls {
				t.Errorf("Expected numberOfRolls to be %d, but got %d results", test.numberOfRolls, len(result.Result))
			}
		}
	}
}
