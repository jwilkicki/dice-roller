package main

import (
	"flag"

	"github.com/jwilkicki/dice-roller/http"
)

var portFlag = flag.Int("p", 8080, "Set the port for the service")

func main() {
	http.Run(*portFlag)
}
