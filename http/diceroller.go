package http

import (
	"fmt"
	"log"
	"net/http"
	"sync"

	"github.com/jwilkicki/dice-roller/dice"
	"github.com/jwilkicki/dice-roller/json"

	"github.com/go-chi/chi/middleware"

	"github.com/go-chi/chi"
)

// Run starts a new dice roller server on the specified port
func Run(port int) {
	router := chi.NewRouter()
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)

	router.Post("/v1/roll", rollDice)

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), router))
}

func rollDice(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received roll request")
	rollRequest, err := json.NewRollRequest(r.Body)

	if err != nil {
		http.Error(w, fmt.Sprintf("Invalid request: %s", err), http.StatusBadRequest)
		return
	}

	log.Printf("Parsed request: %v", rollRequest)

	var workGroup sync.WaitGroup
	workGroup.Add(len(rollRequest))

	groupChannels := make(map[string]chan dice.RollResult)

	for group, spec := range rollRequest {
		numberOfRolls := spec.NumberOfRolls

		groupChannel := make(chan dice.RollResult)
		groupChannels[group] = groupChannel

		currentDie, err := dice.NewDie(spec.Sides)
		if err != nil {
			http.Error(w, fmt.Sprintf("Invalid die for group %s: %d: %v", group, spec.Sides, err), http.StatusBadRequest)
			return
		}
		log.Printf("Starting roll thread for group %s", group)

		go func(rolls int, responseChannel chan dice.RollResult) {
			defer workGroup.Done()
			log.Printf("About to start rolls")
			go currentDie.Roll(rolls, responseChannel)
		}(numberOfRolls, groupChannel)
	}
	log.Print("Waiting for results")
	workGroup.Wait()
	log.Print("Results finished")
	rollResponse := make(json.RollResponse)

	for poolName, resultChan := range groupChannels {
		result := <-resultChan
		log.Printf("Received result for pool %s", poolName)
		if result.Err != nil {
			http.Error(w, fmt.Sprintf("Unable to create results for group %s: %v", poolName, result.Err), http.StatusInternalServerError)
			return
		}

		rollResponse[poolName] = result.Result
	}

	w.Header().Set("Content-Type", "application/json")

	rollResponse.ToJSON(w)
}
